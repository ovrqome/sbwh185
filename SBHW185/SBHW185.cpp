﻿#include <iostream>
#include <stack>
#include <string>

using namespace std;

class Stack
{
private:
    int   size;
    int* arr;

public:
    Stack()
    {
        size = 0;
        arr = nullptr;
    }

    void push(int value)
    {
        int* newArray = new int[size + 1];
        for (int i = 0; i < size; i++)
        {
            newArray[i] = arr[i];
        }
        size++;
        newArray[size - 1] = value;
        delete[] arr;
        arr = newArray;
    }

    int pop()
    {
        int TopElement = arr[size - 1]; 
        size--;
        int* newArray = new int[size];
        for (int i = 0; i < size; i++)
        {
            newArray[i] = arr[i];
        }
        delete[] arr;
        arr = newArray;

        return TopElement;
    }

    void show()
    {
        for (int i = 0; i < size; i++)
        {
            cout << arr[i] << "\t";
        }
        cout << endl;
    }
};

int main()
{
	setlocale(LC_ALL, "rus");

	Stack S;

	S.push(1); S.push(2); S.push(3);
	S.show();
    cout << "удалили" << "\n" << S.pop() << "\n" << "осталось" << endl;
    S.show();

	return 0;
		
}